#!/usr/bin/env python

"""Helena's excellent Python game."""

# Lesson 1: chaos is the way of things
from random import random

# Lesson 2: modern software engineering doesn't care about backwards-compatibility
try:
    input = raw_input
except NameError:
    pass


# Lesson 3: subroutines are handy
def numberOfGuesses(value):
    """Print a number of guesses in a nice way."""
    if value == 1:
        return "1 guess"
    return "{} guesses".format(value)


# Lesson 4: keep on trying
try:
    # If you take more than this many guesses, you lose!
    lose = 5

    print("Press ctrl-C to quit this excellent program.")

    while (1):
        # Ask for the range of numbers we're going to guess between...
        maximum = int(input("\nWhat number would you like to go up to? "))

        print("\nOK. Guess a number between 1 and {}".format(maximum))

        # Pick a target number...
        answer = 1 + int(random() * maximum)

        # Now give the player 'count' goes...
        for count in range(lose):
            remain = lose - count
            guess = int(input("What is your guess (you have {} left)? ".format(numberOfGuesses(remain))))

            # Did they guess right?
            if answer == guess:
                print("\nYou did it - hooray! It took you {}".format(numberOfGuesses(count + 1)))
                break
            elif answer > guess * 2:
                print("It's waaaaay bigger than that!")
            elif answer > guess:
                print("No! It's bigger than that!")
            elif answer < guess * 0.5:
                print("It's a lot smaller!")
            elif answer < guess:
                print("It's smaller!")

        if answer != guess:
            # The player ran out of goes...
            print("Argh! You took too many guesses :( The answer was {}".format(answer))
except KeyboardInterrupt:
    print("\nFinished!")
except:
    pass
